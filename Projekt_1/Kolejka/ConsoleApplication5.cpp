﻿// ConsoleApplication5.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include "pch.h"
#include "queue.h"

// main function
int main()
{
	// create a queue of capacity 4
	queue<string> q(4);

	q.enqueue("a");
	q.enqueue("b");
	q.enqueue("c");

	cout << "Pierwszy element to: " << q.peek() << endl;
	q.dequeue();

	q.enqueue("d");

	cout << "Wielkosc kolejki to: " << q.size() << endl;

	q.dequeue();
	q.dequeue();
	q.dequeue();

	if (q.isEmpty())
		cout << "Kolejka jest pusta\n";
	else
		cout << "Kolejka nie jest pusta\n";

	return 0;
}