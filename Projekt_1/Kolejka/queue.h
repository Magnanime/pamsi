#pragma once
#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

// define default capacity of the queue
#define SIZE 10

// Class for queue
template <class X>
class queue
{
	X *arr;			// tablica zapamietujaca
	int capacity;	// pojemnosc kolejki
	int front;		// wskazuje na pierwszy element kolejki
	int rear;		// wskazuje na ostatni element kolejki
	int count;		// aktualna wielkosc kolejki

public:
	queue(int size = SIZE);		// konstruktor

	void dequeue();
	void enqueue(X x);
	X peek();
	int size();
	bool isEmpty();
	bool isFull();
};

// Constructor to initialize queue
template <class X>
queue<X>::queue(int size)
{
	arr = new X[size];
	capacity = size;
	front = 0;
	rear = -1;
	count = 0;
}

// Usuwanie elementu z kolejki
template <class X>
void queue<X>::dequeue()
{
	// sprawdzam, czy pusta
	if (isEmpty())
	{
		cout << "Kolejka pusta\nZakonczono program\n";
		exit(EXIT_FAILURE);
	}

	front = (front + 1) % capacity;
	count--;
}

// dodaje do kolejki
template <class X>
void queue<X>::enqueue(X item)
{
	// sprawdzam, czy pelna
	if (isFull())
	{
		cout << "Kolejka pelna\nZakonczono program\n";
		exit(EXIT_FAILURE);
	}

	rear = (rear + 1) % capacity;
	arr[rear] = item;
	count++;
}

// Zwracam przedni element kolejki
template <class X>
X queue<X>::peek()
{
	if (isEmpty())
	{
		cout << "Kolejka pusta\nZakonczono program\n";
		exit(EXIT_FAILURE);
	}
	return arr[front];
}

// Zwraca wielkosc kolejki
template <class X>
int queue<X>::size()
{
	return count;
}

// Sprawdzanie czy kolejka jest pusta
template <class X>
bool queue<X>::isEmpty()
{
	return (size() == 0);
}

// jest pelna?
template <class X>
bool queue<X>::isFull()
{
	return (size() == capacity);
}