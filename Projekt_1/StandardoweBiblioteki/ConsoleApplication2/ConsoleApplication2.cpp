﻿// ConsoleApplication2.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include "pch.h"
#include <iostream>
#include <stack>  
#include <queue>
#include <list>

int main()
{

	std::cout << "Dzien dobry z programu" << std::endl<<std::endl;
	std::cout << "Tutaj std::stack" << std::endl;
	
	std::stack<int> stos;
	for (int i = 0; i < 5; ++i) {
		stos.push(i);
	}

	std::cout << "Wielkosc stosu" << std::endl;

	std::cout << stos.size() << std::endl;

	std::cout << "Element u gory" << std::endl;

	std::cout << stos.top() << std::endl;

	std::cout << "Usuwam 3 elementy" << std::endl;
	int r = 3;
	if (stos.size() < r) { std::cout << "Blad, za maly stos" << std::endl; }
	else {
		for (int i = 0; i < r; ++i) {
			stos.pop();
		}
	}

	int k = 9;

	std::cout << "Dodaje element" << std::endl;

	stos.push(k);

	std::cout << "Wielkosc stosu" << std::endl;

	std::cout << stos.size()<<std::endl;

	std::cout << std::endl << "Tutaj std::queue" << std::endl;
	std::queue < int > kolejka;
	for (int i = 0; i < 5; ++i) {
		kolejka.push(i);
	}

	std::cout << "Pierwszy element" << std::endl;
	std::cout << kolejka.front() << std::endl;
	std::cout << "Usuwam 2 elementy z poczatku" << std::endl;

	int l = kolejka.size();
	int o = 2;
	if (l < o) { std::cout << "Blad, kolejka za mala" << std::endl; }
	else {
		for (int i = 0; i < o; ++i) {
			kolejka.pop();
		}
	}

	std::cout << "Dodaje 3 elementy na koniec" << std::endl;
	for (int i = 0; i < 3; ++i) {
		kolejka.push(i*3);
	}

	std::cout << "Pierwszy element" << std::endl;
	std::cout << kolejka.front() << std::endl;
	std::cout << "Liczba elementow" << std::endl;
	std::cout << kolejka.size() << std::endl;




	std::cout << std::endl << "Tutaj std::list" << std::endl;

	std::list<int> lista;
	for (int i = 0; i < 5; ++i) {
		lista.push_back(i);
	}

	int s = lista.size();

	std::cout << "Zawartosc listy" << std::endl;

	for (s; s > 0; s--) {
		int tmp = lista.front();
		std::cout << lista.front();
		lista.pop_front();
		lista.push_back(tmp);
	}

	std::cout << std::endl;

	std::cout << "Dodaje na poczatek 2" << std::endl;

	for (int i = 0; i < 2; i++) {
		lista.push_front(i*6);
	}

	s = lista.size();

	std::cout << "Zawartosc listy" << std::endl;

	for (s; s > 0; s--) {
		int tmp = lista.front();
		std::cout << lista.front();
		lista.pop_front();
		lista.push_back(tmp);
	}
	std::cout << std::endl;
	std::cout << "Usuwam z poczatku 3" << std::endl;

	r = 3;
	if (lista.size() < r) { std::cout << "Blad, za mala lista" << std::endl; }
	else {
		for (int i = 0; i < r; i++) {
			lista.pop_front();
		}
	}
	s = lista.size();

	std::cout << "Zawartosc listy" << std::endl;

	for (s; s > 0; s--) {
		int tmp = lista.front();
		std::cout << lista.front();
		lista.pop_front();
		lista.push_back(tmp);
	}

	std::cout << std::endl;

	std::cout << "Poczatek listy" << std::endl;

	std::cout << lista.front() << std::endl;

	std::cout << "Usuwam z konca 2" << std::endl;
	r = 2;
	if (lista.size() < r) { std::cout << "Blad, za mala lista" << std::endl; }
	else {
		for (int i = 0; i < 2; i++) {
			lista.pop_back();
		}
	}
	s = lista.size();

	std::cout << "Zawartosc listy" << std::endl;

	for (s; s > 0; s--) {
		int tmp = lista.front();
		std::cout << lista.front();
		lista.pop_front();
		lista.push_back(tmp);
	}

	std::cout << std::endl;

	std::cout << "Koniec listy" << std::endl;

	std::cout << lista.back() << std::endl;

	s = lista.size();

	std::cout << "Zawartosc listy" << std::endl;

	for (s; s > 0; s--) {
		int tmp = lista.front();
		std::cout << lista.front();
		lista.pop_front();
		lista.push_back(tmp);
	}

	std::cout << std::endl;

	s = lista.size();

	std::cout << "Usuwanie wszystkiego" << std::endl;

	for (s; s > 0; s--) {
		lista.pop_front();
	}

	s = lista.size();

	std::cout << "Zawartosc listy" << std::endl;
	if (s == 0) { std::cout << "Pusto" << std::endl; }
	else {
		for (s; s > 0; s--) {
			int tmp = lista.front();
			std::cout << lista.front();
			lista.pop_front();
			lista.push_back(tmp);
		}
	}
	std::cout << std::endl;



	return 0;
}

