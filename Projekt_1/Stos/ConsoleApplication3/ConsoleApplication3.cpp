﻿// ConsoleApplication3.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include "pch.h"
#include "stos.h"
#include <iostream>
#include <string>

int main()
{
    std::cout << "Dzien dobry z programu2\n"; 
	stack<string> pt(5); // inicjalizacja stosu o wielkosci 5

	//Dodaje do stosu A i B

	std::cout << "Dodaje do stosu A i B" << std::endl;

	pt.push("A");
	pt.push("B");

	std::cout << "Odejmuje od stosu B" << std::endl;

	pt.pop();

	pt.push("C");

	// Prints the top of the stack
	cout << "Element na gorze to: " << pt.peek() << endl;

	// Liczba elementow na stosie
	cout << "Wielkosc stosu to " << pt.size() << endl;

	pt.pop();

	// stos pusty?
	if (pt.isEmpty())
		cout << "Stos jest pusty\n";
	else
		cout << "Stos nie jest pusty\n";

	return 0;
}
