#pragma once
#include <iostream>
#include <cstdlib>
using namespace std;

// Domyslna wielkosc stosu 
#define SIZE 10

// Class for stack
template <class X>
class stack
{
	X *tab;
	int top;
	int capacity;

public:
	stack(int size = SIZE);	// inicjalizacja bez parametru = inicjalizacja do 10

	void push(X);
	X pop();
	X peek();

	int size();
	bool isEmpty();
	bool isFull();
};

// Constructor to initialize stack
template <class X>
stack<X>::stack(int size)
{
	tab = new X[size]; //tablica przechowywalna
	capacity = size;
	top = -1;
}

// funkcja pushujaca
template <class X>
void stack<X>::push(X x)
{
	if (isFull())
	{
		cout << "Przepelnienie stosu\nKoniec programu\n";
		exit(EXIT_FAILURE);
	}
	tab[++top] = x;
}

// funkcja popujaca
template <class X>
X stack<X>::pop()
{
	// sprawdzam, czy nie pusty
	if (isEmpty())
	{
		cout << "Stos pusty!\nZakonczenie programu\n";
		exit(EXIT_FAILURE);
	}

	// decrease stack size by 1 and (optionally) return the popped element
	return tab[top--];
}

// zwracanie topowego elementu stosu
template <class X>
X stack<X>::peek()
{
	if (!isEmpty())
		return tab[top];
	else
		exit(EXIT_FAILURE);
}

// zwracanie wielkosci stosu
template <class X>
int stack<X>::size()
{
	return top + 1;
}

// stos pusty, czy nie?
template <class X>
bool stack<X>::isEmpty()
{
	return top == -1;	// zwraca pozycje topa
}

// stos pelny, czy nie?
template <class X>
bool stack<X>::isFull()
{
	return top == capacity - 1;	// zwraca pozycje topa
}