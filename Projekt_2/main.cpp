#include <iostream>
#include "algorytmy_sortowania.h"
#include <fstream>

using namespace std;


void Pokaz_wiadomosc_scalanie();
void Pokaz_wiadomosc_qs();
void Pokaz_wiadomosc_is();

int main()
{
	// czyszczenie zawartosci pliku, przed zapisem
	std::fstream plik;
	plik.open("pomiary.txt", std::ofstream::out | std::ofstream::trunc);
	plik.close();
	cout.precision(15);

    cout << "Program sortujacy tablice roznymi algorytmami sortowania." << endl;
    cout << "Wykorzystano algorytm QuickSort, IntroSort i sortowanie przez scalanie" << endl;
    cout << "Troche to potrwa :(" << endl;
    cout << "Nacisnij ENTER aby rozpoczac" << endl;	
    cin.get();

	Pokaz_wiadomosc_scalanie();
	Wykonaj_pomiary_sortowanie_scalanie1();         //Tutaj scalanie dla losowych
	Wykonaj_pomiary_sortowanie_scalanie2();         //Tutaj scalanie dla poukładanych odwrotnie

	Wykonaj_pomiary_sortowanie_scalanie3(25);       //Tutaj 25% posortowanie
	Wykonaj_pomiary_sortowanie_scalanie3(50);       //Tutaj 50% posortowanie
	Wykonaj_pomiary_sortowanie_scalanie3(75);       //Tutaj 75% posortowanie
	Wykonaj_pomiary_sortowanie_scalanie3(95);       //Tutaj 95% posortowanie
	Wykonaj_pomiary_sortowanie_scalanie3(99);       //Tutaj 99% posortowanie
	Wykonaj_pomiary_sortowanie_scalanie3(99.7);     //Tutaj 99.7% posortowanie
	
	Pokaz_wiadomosc_qs();
	Wykonaj_pomiary_sortowanie_quicksort1();        //Tutaj QS dla losowych
	Wykonaj_pomiary_sortowanie_quicksort2();        //Tutaj QS dla poukładanych odwrotnie

	Wykonaj_pomiary_sortowanie_quicksort3(25);      //Tutaj 25% posortowane
	Wykonaj_pomiary_sortowanie_quicksort3(50);      //Tutaj 50% posortowanie
	Wykonaj_pomiary_sortowanie_quicksort3(75);      //Tutaj 75% posortowanie
	Wykonaj_pomiary_sortowanie_quicksort3(95);      //Tutaj 95% posortowanie
	Wykonaj_pomiary_sortowanie_quicksort3(99);      //Tutaj 99% posortowanie
	Wykonaj_pomiary_sortowanie_quicksort3(99.7);    //Tutaj 99.7% posortowanie

	Pokaz_wiadomosc_is();
	Wykonaj_pomiary_sortowanie_introsort1();        //Tutaj IS dla losowych
	Wykonaj_pomiary_sortowanie_introsort2();        //Tutaj IS dla poukładanych odwrotnie

	Wykonaj_pomiary_sortowanie_introsort3(25);      //Tutaj 25% posortowanie
	Wykonaj_pomiary_sortowanie_introsort3(50);      //Tutaj 50% posortowanie
	Wykonaj_pomiary_sortowanie_introsort3(75);      //Tutaj 75% posortowanie
	Wykonaj_pomiary_sortowanie_introsort3(95);      //Tutaj 95% posortowanie
	Wykonaj_pomiary_sortowanie_introsort3(99);      //Tutaj 99% posortowanie
	Wykonaj_pomiary_sortowanie_introsort3(99.7);    //Tutaj 99.7% posortowanie
	
	cout << "Wreszcie! W katalogu utworzony zostal plik z danymi dotyczacymi poszczegolnych sortowan" << endl;
    cout << "Nacisnij ENTER aby zakonczyc" << endl;
	cin.get();
	return 0;
}



void Pokaz_wiadomosc_scalanie()
{
	cout << "*********************************************************" << endl;
	cout << "***      ROZPOCZYNAM SORTOWANIE PRZEZ SCALANIE        ***" << endl;
	cout << "*********************************************************" << endl;
	cout << endl;
}
void Pokaz_wiadomosc_qs()
{
	cout << endl;
	cout << "*********************************************************" << endl;
	cout << "***               ROZPOCZYNAM QUICKSORT               ***" << endl;
	cout << "*********************************************************" << endl;
	cout << endl;
}
void Pokaz_wiadomosc_is()
{
	cout << endl;
	cout << "*********************************************************" << endl;
	cout << "***              ROZPOCZYNAM INTROSORT                ***" << endl;
	cout << "*********************************************************" << endl;
	cout << endl;
}






